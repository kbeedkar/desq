package de.uni_mannheim.informatik.dws.desq.example;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.api.java.function.Function;

import de.uni_mannheim.informatik.dws.desq.utils.SequenceContext;

/**
 * Example function for API.
 * 
 * @author Axel Rantil (axel@rantil.se)
 *
 */
public class ExampleFunction implements Function<String, SequenceContext>{

	private Map<String, String> getParentMap(){
		Map<String, String> parents = new HashMap<String, String>();
		parents.put("DSLR_Camera","Camera");
		parents.put("PointShoot_Camera","Camera");
		parents.put("On_Photography","Photography_Book");
		parents.put("The_Digital_Photography","Photography_Book");
		parents.put("The_Art_Of_Photography","Photography_Book");
		parents.put("SanDisk_Extreme_32GB","Flash");
		parents.put("Ravelli_APGL5","Tripod");
		parents.put("Olympus_SX410","PointShoot_Camera");
		parents.put("Nikon_D5300","DSLR_Camera");
		parents.put("Canon_EOS_70D","DSLR_Camera");
		parents.put("Kingston_8GB_microSDHC","Flash");
		parents.put("Dolica_GX600B200","Tripod");
		return parents;
	}

	@Override
	public SequenceContext call(String str) throws Exception {
		SequenceContext sqc = new SequenceContext();
		Map<String, String> parentMap = getParentMap();
		Boolean knownItem;
		Boolean hasParent;
		Boolean knownParent;
		String[] words = str.split(" ");
		for (int i = 0; i < words.length; i++) {
			String word = words[i];
			sqc.addToSequence(word);
			knownItem = sqc.addToHierarchy(word);
			if (!knownItem) {
				while (true) {
					hasParent = parentMap.containsKey(word);
					if (hasParent) {
						knownParent = sqc.addToHierarchy(word, parentMap.get(word));
						if (!knownParent) {
							word = parentMap.get(word);
						} else {
							break;
						}
					} else {
						break;
					}
				}
			}
			if (i == 3 || i == 7) { //To get several sequences for each partition
				sqc.flushSequence();
			}
		}
		sqc.flushSequence();
		return sqc;
	}


}