package de.uni_mannheim.informatik.dws.desq;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;

import scala.Tuple2;
import de.uni_mannheim.informatik.dws.desq.fst.Fst;
import de.uni_mannheim.informatik.dws.desq.fst.XFst;
import de.uni_mannheim.informatik.dws.desq.example.ExampleFunction;
import de.uni_mannheim.informatik.dws.desq.mining.DesqCount;
import de.uni_mannheim.informatik.dws.desq.mining.OnePassIterative;
import de.uni_mannheim.informatik.dws.desq.patex.PatEx;
import de.uni_mannheim.informatik.dws.desq.utils.DesqDataset;
import de.uni_mannheim.informatik.dws.desq.utils.DesqResult;
import de.uni_mannheim.informatik.dws.desq.utils.Hierarchy;
import de.uni_mannheim.informatik.dws.desq.utils.Sequence;
import de.uni_mannheim.informatik.dws.desq.utils.SequenceContext;

public class Desq implements Serializable {

	static Logger LOGGER = Logger.getLogger(Desq.class.getName());

	static XFst xFst;
	static DesqCount DC;

	
	/**
	 * API for Desq. Builds a DesqDataset in a parallel fashion according to the provided function
	 * @param sc
	 * @param rawData 
	 * 				RDD of sequences
	 * @param providedFunction 
	 * 				function developed according to the contract of the API
	 * @return DesqDataset ready for mining
	 */
	public static <T> DesqDataset buildDesqDataset(JavaSparkContext sc, JavaRDD<T> rawData, Function<T, SequenceContext> providedFunction){
		
		JavaRDD<SequenceContext> mappedData = rawData.map(providedFunction);
		
		JavaRDD<SequenceContext> mappedAndFlushedData = mappedData.map(new Function<SequenceContext, SequenceContext>(){
					@Override
					public SequenceContext call(SequenceContext scq) throws Exception {
						if (!scq.isFlushed()){
							scq.flushSequence();
						}
						return scq;
					}
		});
		
		//printSequenceContexts(mappedAndFlushedData); //Debugging output
		
		SequenceContext reducedData = mappedAndFlushedData.reduce(new ReduceSequenceContexts());
		return reducedData.getDD(sc);
	}

	
	static class ReduceSequenceContexts implements Function2<SequenceContext, SequenceContext, SequenceContext> {
		@Override
		public SequenceContext call(SequenceContext sqc1, SequenceContext sqc2) throws Exception {
			sqc1.mergeSqc(sqc2);
			return sqc1;
		}
	}
	
	//Debug output: to be removed
	public static void printSequenceContexts(JavaRDD<SequenceContext> mappedData) {
		List<SequenceContext> collectedData = mappedData.collect();
		int partition = 1;
		for (SequenceContext partitions : collectedData){
			partitions.print(partition++);
		}
	}
	
	/**
	 * @return returns an example function for buildHierarchy
	 */
	public static ExampleFunction getExampleFunction(){
		return new ExampleFunction();
	}
	
	/**
	 * Loads a hierarchy instance from a CSV-file
	 * 
	 * @param pathHier 
	 * 				path to the CSV dictionary
	 * @return hierarchy instance without statistics
	 * @throws IOException
	 */
	public static Hierarchy loadHierarchyCsv(JavaSparkContext sc, String pathHier) {
		
		Hierarchy hier = new Hierarchy();
		hier.loadCSV(sc, pathHier);
		return hier;
		
	}
	
	/**
	 * Loads a hierarchy instance without statistics from a JSON-file
	 * 
	 * @param pathHier 
	 * 				path to JSON dictionary
	 * @return hierarchy instance without statistics
	 * @throws IOException
	 */
	public static Hierarchy loadHierarchyJson(JavaSparkContext sc, String pathHier) {

		Hierarchy hier = new Hierarchy();
		hier.loadJSON(sc, pathHier, false); 
		return hier;
	}
		
	/**
	 * Creates a DesqDataset containing a hierarchy instance with computed statistics and an RDD of Strings
	 * 
	 * @param hier 
	 * 				a hierarchy instance without statistics
	 * @param labelSeq 
	 * 				RDD of label's
	 * @return DesqDataset with label status
	 */
	public static DesqDataset prepareText(Hierarchy hier, JavaRDD<String> labelSeq){
		return new DesqDataset(hier, labelSeq, false);
	}
	
	
	/**
	 * Loads a DesqDataset saved with DesqDataset.save(path)
	 * @param sc
	 * @param path 
	 * 				path to the file provided to DesqDataset.save()
	 * @return DesqDataset with the same content and status as the saved DesqDataset
	 */
	public static DesqDataset loadDesqDataset(JavaSparkContext sc, String path) {
		DesqDataset dd = new DesqDataset();
		dd.load(sc, path);
		return dd;
	}
	
	/**
	 * Searches a JavaRDD for occurrences of the passed pattern and returns an
	 * RDD of sequences matching to the pattern along with the number of their
	 * occurence.
	 * 
	 * @param sc
	 *            spark context
	 * @param sequences
	 *            rdd containing the sequences to search in
	 * @param dict
	 *            dictionary and hierarchy object
	 * @param pattern
	 *            the pattern to look for
	 * @param support
	 *            minimum support
	 * @return
	 */
	public static DesqResult mine(DesqDataset desqDataset, String pattern, int support) {
		
		Map<String, String> conf = new HashMap<String, String>();
		return mine(desqDataset, pattern, support, conf);
	}

	/**
	 * Searches a JavaRDD for occurrences of the passed pattern and returns an
	 * RDD of sequences matching to the pattern along with the number of their
	 * occurrence.
	 * 
	 * @param sc
	 *            spark context
	 * @param sequences
	 *            rdd containing compressed sequences
	 * @param dict
	 *            dictionary and hierarchy object
	 * @param pattern
	 *            the pattern to look for
	 * @param support
	 *            minimum support
	 * @param conf
	 *            a map containing additional configuration variables
	 * @return an RDD containing occurrences of the pattern along with their
	 *         frequency
	 */
	public static DesqResult mine(DesqDataset desqDataset, String pattern, final int support,
			Map<String, String> conf) {
		

		if (!desqDataset.isPrepared())
			desqDataset.encode("fid");

		final Hierarchy hier = desqDataset.getHierarchy();
		JavaRDD<Sequence> seq = desqDataset.getSequences();

		/** Generate the automaton */
		pattern = ".* [" + pattern.trim() + "]";
		PatEx ex = new PatEx(pattern, hier);
		Fst cFst = ex.translateToFst();
		xFst = cFst.optimizeForExecution(); // was: (hier);

		// TODO: now we store the dict in pFST and in DesqCount, which is not
		// ideal.

		/**
		 * Create the DesqCount object, which we will ship around to the worker
		 * nodes
		 */
		
		// todo: use flist. should work without further work
		DC = new OnePassIterative(support, xFst, hier, false);

		/** Mine the sequences on the worker nodes */
		JavaPairRDD<Sequence, Integer> sequencesWithCount = seq.flatMapToPair(new MineSequence());

		/** Reduce the occurrences */
		JavaPairRDD<Sequence, Integer> sequencesWithCount_Reduced = sequencesWithCount
				.reduceByKey(new Function2<Integer, Integer, Integer>() {
					public Integer call(Integer a, Integer b) {
						return a + b;
					}
				});

		/** Filter the occurrences by minimum support */
		JavaPairRDD<Sequence, Integer> sequencesWithCount_ReducedAndFiltered = sequencesWithCount_Reduced
				.filter(new Function<Tuple2<Sequence, Integer>, Boolean>() {
					public Boolean call(Tuple2<Sequence, Integer> occurence) {
						return occurence._2 >= support;
					}
				});

		// Translate sequences back to id's
		JavaRDD<Tuple2<Sequence, Integer>> sequencesWithCount_ReducedAndFiltered_translatedBack = sequencesWithCount_ReducedAndFiltered
				.map(new Function<Tuple2<Sequence, Integer>, Tuple2<Sequence, Integer>>() {
					@Override
					public Tuple2<Sequence, Integer> call(Tuple2<Sequence, Integer> sequence) {
						return new Tuple2<Sequence, Integer>(hier.translateToIds(sequence._1), sequence._2);
					}
				});

		return new DesqResult(hier, sequencesWithCount_ReducedAndFiltered_translatedBack);
	}

	static class MineSequence implements PairFlatMapFunction<Sequence, Sequence, Integer>, Serializable {

		public Iterable<Tuple2<Sequence, Integer>> call(Sequence sequence) throws Exception {

			/** Use DfsCount to mine the support */
			ArrayList<Tuple2<Sequence, Integer>> matches = DC.scanOneSeq(sequence);

			// Debug output
			// for(Tuple2<String, Integer> tuple : matches) {
			// LOGGER.error("Found in [" + sequence + "]: [" + tuple._1 + "]
			// WITH count " + tuple._2);
			// }

			return matches;
		}
	}
	
	/**
	 * Loads a DesqResult saved with DesqResult.save(path)
	 * 
	 * @param sc
	 * @param path 
	 * 				given as an argument for the DesqResult.save()-method
	 * @return DesqResult with the same content and status as the saved DesqResult
	 */
	public static DesqResult loadDesqResult(JavaSparkContext sc, String path){
		
		DesqResult dr = new DesqResult();
		dr.load(sc, path);
		return dr;
		
	}

	/**
	 * Convert a string of integers to a integer array
	 * 
	 * @param s
	 * @return
	 */
	public static int[] stringToInts(String s) {
		String[] sa = s.split(" ");
		int[] num = new int[sa.length];
		for (int i = 0; i < num.length; i++) {
			num[i] = Integer.parseInt(sa[i]);
		}
		return num;
	}

	/**
	 * Compress a given sequence of integers using a variable-length format
	 * 
	 * @param seq
	 *            the input sequence
	 * @return variable-length representation of the input sequence
	 * @throws IOException
	 */
	public static Sequence compressSequence(int[] seq) {
		return new Sequence(seq);
	}

	/**
	 * Decompress a variable-length format byte sequence to an array of integers
	 * 
	 * @param b
	 *            variable-length format of an integer sequence as ByteArray
	 * @return uncompressed integer sequence
	 * @throws IOException
	 */
	public static int[] decompressSequence(Sequence b) {
		return b.decompressSequence();
	}

	/**
	 * Convert an array of labels to a string of labels
	 * 
	 * @param
	 * @return
	 */
	// Used for dd encoding, query and decoding
	public static String labelToString(String[] seq) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < seq.length; i++) {
			sb.append(seq[i]);

			if (i != seq.length - 1) {
				sb.append(" ");
			}
		}

		return sb.toString();
	}

	// Used for temporary input pattern and decodeToIds
	public static String intsToString(int[] seq) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < seq.length; i++) {
			sb.append(seq[i]);
			if (i != seq.length - 1) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}
	
	/**
	 * Returns a DesqDataset prepared for mining
	 * 
	 * @param sc
	 * @param pathSeq path to a file with seq of fid's
	 * @param pathHier path to a file with the old dictionary format
	 * @return a DesqDataset
	 * @throws IOException
	 */
	public static DesqDataset loadOld(JavaSparkContext sc, String pathSeq, String pathHier) throws IOException {

		Hierarchy hier = new Hierarchy();
		hier.loadOld(pathHier);

		JavaRDD<String> stringSeq = sc.textFile(pathSeq);
		JavaRDD<Sequence> seq = stringSeq.map(new Function<String, Sequence>(){
			@Override
			public Sequence call(String str) throws Exception {
				String[] stra = str.split(" ");
				int[] inta = new int[stra.length];
				return compressSequence(inta);
			}
		});
		DesqDataset dd = new DesqDataset(hier, seq);
		return dd;
	}
	
	public static DesqResult mineOld(DesqDataset desqDataset, String pattern, int support) {
		Map<String, String> conf = new HashMap<String, String>();
		return mine(desqDataset, pattern, support, conf);
	}

}
