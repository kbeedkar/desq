package de.uni_mannheim.informatik.dws.desq.mining;


import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2LongOpenCustomHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import de.uni_mannheim.informatik.dws.desq.fst.OutputLabel;
import de.uni_mannheim.informatik.dws.desq.fst.XFst;
import de.uni_mannheim.informatik.dws.desq.utils.Hierarchy;

public class OnePassIterative extends DesqCount {

	// We can't use any class variables in Spark, as they are shared amongst 
	// threads on the same worker machine
	
	// Buffer to store output sequences
	//IntArrayList buffer = new IntArrayList();

	// Hashset to store ancestors
	//IntOpenHashSet tempAnc = new IntOpenHashSet();

	//int pos = 0;
	
	//ObjectArrayList<Node>[] statePrefix;
	//int[] stateList;
	//int stateListSize = 0;
	

	int numStates = 0;

	int initialState = 0;

	class Node {
		int item;

		ObjectArrayList<Node> prefixes;

		Node(int item, ObjectArrayList<Node> suffixes) {
			this.item = item;
			this.prefixes = suffixes;
		}
	}
	
	class State {
		ObjectArrayList<Node>[] statePrefix;
		int[] stateList;
		int stateListSize = 0;
		
		public State(int numStates, int initialState) {
			statePrefix = (ObjectArrayList<Node>[]) new ObjectArrayList[numStates];
			stateList = new int[numStates];;
			stateListSize = 0;
			
			statePrefix[initialState] = new ObjectArrayList<Node>();
			stateList[stateListSize++] = initialState;
			statePrefix[initialState].add(null);
		}
	}


	@SuppressWarnings("unchecked")
	public OnePassIterative(int sigma, XFst xfst, Hierarchy hier, boolean useFlist) {
		super(sigma, xfst, hier, useFlist);
		numStates = xfst.numStates();
		//statePrefix = (ObjectArrayList<Node>[]) new ObjectArrayList[numStates];
		//stateList = new int[numStates];
		initialState = xfst.getInitialState();
	}

	/*private void reset() {
		//pos = 0;
		for (int i = 0; i < numStates; ++i) {
			statePrefix[i] = null;
		}
		stateListSize = 0;
	}*/

	@Override
	protected void computeMatch() {};
	
	@Override
	protected void computeMatch(int pos, int[] sequence, Object2LongOpenCustomHashMap<int[]> outputSequences) {
		
		State state = new State(numStates, initialState);
		
		
		IntOpenHashSet tempAnc = new IntOpenHashSet();
		IntArrayList buffer = new IntArrayList();

		while (pos < sequence.length) {
			step(pos, sequence, buffer, state, tempAnc, outputSequences);
			pos++;
		}
	}

	
	
	private void step(int pos, int[] sequence, IntArrayList buffer, State state, IntOpenHashSet tempAnc, Object2LongOpenCustomHashMap<int[]> outputSequences) {
		@SuppressWarnings("unchecked")
		ObjectArrayList<Node>[] nextStatePrefix = (ObjectArrayList<Node>[]) new ObjectArrayList[numStates];
		int[] nextStateList = new int[numStates];
		int nextStateListSize = 0;

		int itemId = sequence[pos];

		for (int i = 0; i < state.stateListSize; i++) {
			int fromState = state.stateList[i];

			if (xfst.hasOutgoingTransition(fromState, itemId)) {
				for (int tId = 0; tId < xfst.numTransitions(fromState); ++tId) {
					if (xfst.canStep(itemId, fromState, tId)) {
						int toState = xfst.getToState(fromState, tId);
						OutputLabel olabel = xfst.getOutputLabel(fromState, tId);

						boolean isFinal = xfst.isFinalState(toState);
						Node node;

						if (null == nextStatePrefix[toState]) {
							nextStatePrefix[toState] = new ObjectArrayList<Node>();
							nextStateList[nextStateListSize++] = toState;
						}

						switch (olabel.type) {
						case EPSILON:
							if (isFinal)
								computeOutput(state.statePrefix[fromState], buffer, outputSequences);
							for (Node n : state.statePrefix[fromState])
								nextStatePrefix[toState].add(n);
							break;

						case CONSTANT:
							int outputItemId = olabel.item;
							if (!useFlist || flist[outputItemId] >= sigma) {
								node = new Node(outputItemId, state.statePrefix[fromState]);
								if (isFinal)
									computeOutput(node, buffer, outputSequences);
								nextStatePrefix[toState].add(node);
							}
							break;

						case SELF:
							if (!useFlist || flist[itemId] >= sigma) {
								node = new Node(itemId, state.statePrefix[fromState]);
								if (isFinal)
									computeOutput(node, buffer, outputSequences);

								nextStatePrefix[toState].add(node);
							}
							break;

						case SELFGENERALIZE:
							IntArrayList stack = new IntArrayList();
							int top = 0;
							int rootItemId = olabel.item;
							stack.add(itemId);
							tempAnc.add(itemId);
							while (top < stack.size()) {
								int currItemId = stack.getInt(top);
								for (int parentId : hier.getParents(currItemId)) {
									if (xfst.isReachable(rootItemId, parentId) && !tempAnc.contains(parentId)) {
										stack.add(parentId);
										tempAnc.add(parentId);
									}
								}
								top++;
							}
							tempAnc.clear();
							for (int id : stack) {
								if (!useFlist || flist[id] >= sigma) {
									node = new Node(id, state.statePrefix[fromState]);
									if (isFinal)
										computeOutput(node, buffer, outputSequences);
									nextStatePrefix[toState].add(node);
								}
							}

							break;
						default:
							break;
						}
					}

				}
			}
		}
		
		state.stateList = nextStateList;
		state.stateListSize = nextStateListSize;
		state.statePrefix = nextStatePrefix;
	}

	private void computeOutput(ObjectArrayList<Node> suffixes, IntArrayList buffer, Object2LongOpenCustomHashMap<int[]> outputSequences) {
		for (Node node : suffixes)
			computeOutput(node, buffer, outputSequences);
	}

	private void outputBuffer(IntArrayList buffer, Object2LongOpenCustomHashMap<int[]> outputSequences) {

		if (!buffer.isEmpty()) {
			countSequence(reverse(buffer.toIntArray()), outputSequences);
		}
	}

	private void computeOutput(Node node, IntArrayList buffer, Object2LongOpenCustomHashMap<int[]> outputSequences) {
		if (node == null) {
			outputBuffer(buffer, outputSequences);
			return;
		}

		buffer.add(node.item);
		for (Node n : node.prefixes) {
			computeOutput(n, buffer, outputSequences);
		}
		buffer.remove(buffer.size() - 1);
	}

	private int[] reverse(int[] a) {
		int i = 0;
		int j = a.length - 1;
		while (j > i) {
			a[i] ^= a[j];
			a[j] ^= a[i];
			a[i] ^= a[j];
			i++;
			j--;
		}
		return a;
	}
}