package de.uni_mannheim.informatik.dws.desq.mining;

import java.util.ArrayList;
import java.util.Map;

import de.uni_mannheim.informatik.dws.desq.Desq;
import de.uni_mannheim.informatik.dws.desq.fst.XFst;
import de.uni_mannheim.informatik.dws.desq.utils.Hierarchy;
import de.uni_mannheim.informatik.dws.desq.utils.IntArrayStrategy;
import de.uni_mannheim.informatik.dws.desq.utils.PrimitiveUtils;
import de.uni_mannheim.informatik.dws.desq.utils.Sequence;
import it.unimi.dsi.fastutil.objects.Object2LongOpenCustomHashMap;
import scala.Tuple2;

/**
 * DesqCount.java
 * 
 * @author Kaustubh Beedkar {kbeedkar@uni-mannheim.de}
 */
public abstract class DesqCount {

	// protected Dictionary dictionary = Dictionary.getInstance();
	Hierarchy hier;

	// protected Object2LongOpenCustomHashMap<int[]> outputSequences = new
	// Object2LongOpenCustomHashMap<int[]>(
	// new IntArrayStrategy());

	protected int sigma;

	protected XFst xfst;

	protected boolean writeOutput = true;

	protected boolean useFlist = true;

	// protected int[] sequence;

	protected int sid = -1;

	protected int numPatterns = 0;

	protected int[] flist;

	// protected SequentialWriter writer = SequentialWriter.getInstance();

	protected long gpt = 0L;

	protected long gptUnique = 0L;

	protected long globalGpt = 0L;

	protected long globalGptUnique = 0L;

	protected long totalMatchedSequences = 0L;

	protected long totalMatches = 0L;

	// Methods

	public DesqCount(int sigma, XFst dfa, Hierarchy hier, boolean useFlist) {
		this.sigma = sigma;
		this.xfst = dfa;
		// this.writeOutput = writeOutput;
		this.hier = hier;
		this.useFlist = useFlist;
		this.flist = hier.getFlist();
	}

	/*
	 * public void scan(String file) throws Exception { FileInputStream fstream
	 * = new FileInputStream(file); DataInputStream in = new
	 * DataInputStream(fstream); BufferedReader br = new BufferedReader(new
	 * InputStreamReader(in));
	 * 
	 * IntArrayList buffer = new IntArrayList();
	 * 
	 * String line; while ((line = br.readLine()) != null) { if
	 * (!line.isEmpty()) { String[] str = line.split("\\s* \\s*"); sequence =
	 * new int[str.length]; for (int i = 0; i < str.length; ++i) { sequence[i] =
	 * Integer.parseInt(str[i]); }
	 * 
	 * sid = sid + 1; gpt = 0; gptUnique = 0; buffer.clear();
	 * //computeMatch(buffer, 0, dfa.getInitialState()); computeMatch();
	 * globalGpt += gpt; globalGptUnique += gptUnique;
	 * 
	 * if(gpt > 0) totalMatchedSequences++; } } br.close();
	 * 
	 * totalMatches = outputSequences.size();
	 * 
	 * // output all frequent sequences for (Map.Entry<int[], Long> entry :
	 * outputSequences.entrySet()) { long value = entry.getValue(); int support
	 * = PrimitiveUtils.getLeft(value); if (support >= sigma) { numPatterns++;
	 * if(writeOutput) { writer.write(entry.getKey(), support); } } } }
	 */

	public ArrayList<Tuple2<Sequence, Integer>> scanOneSeq(Sequence sequence_compressed) throws Exception {

		int[] sequence = Desq.decompressSequence(sequence_compressed);

		sid = sid + 1;
		gpt = 0;
		gptUnique = 0;
		Object2LongOpenCustomHashMap<int[]> outputSequences = new Object2LongOpenCustomHashMap<int[]>(
				new IntArrayStrategy());

		// computeMatch(buffer, 0, dfa.getInitialState());
		computeMatch(0, sequence, outputSequences);
		globalGpt += gpt;
		globalGptUnique += gptUnique;

		if (gpt > 0)
			totalMatchedSequences++;

		totalMatches = outputSequences.size();

		ArrayList<Tuple2<Sequence, Integer>> matches = new ArrayList<Tuple2<Sequence, Integer>>();
		// output all frequent sequences
		for (Map.Entry<int[], Long> entry : outputSequences.entrySet()) {
			long value = entry.getValue();
			int support = PrimitiveUtils.getLeft(value);
			if (support >= sigma) {
				numPatterns++;

				matches.add(new Tuple2<Sequence, Integer>(Desq.compressSequence(entry.getKey()), support));
			}
		}

		return matches;
	}

	protected abstract void computeMatch();

	protected abstract void computeMatch(int pos, int[] sequence, Object2LongOpenCustomHashMap<int[]> outputSequences);

	protected void countSequence(int[] sequence, Object2LongOpenCustomHashMap<int[]> outputSequences) {
		gpt++;
		Long supSid = outputSequences.get(sequence);
		if (supSid == null) {
			outputSequences.put(sequence, PrimitiveUtils.combine(1, sid));
			gptUnique++;
			return;
		}
		if (PrimitiveUtils.getRight(supSid) != sid) {
			int newCount = PrimitiveUtils.getLeft(supSid) + 1;
			outputSequences.put(sequence, PrimitiveUtils.combine(newCount, sid));
			gptUnique++;
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public long getGlobalGpt() {
		return globalGpt;
	}

	public long getTotalMatchedSequences() {
		return totalMatchedSequences;
	}

	public long getGlobalGptUnique() {
		return globalGptUnique;
	}

	public int noOutputPatterns() {
		return numPatterns;
	}

	public long getTotalMatches() {
		return totalMatches;
	}

}
