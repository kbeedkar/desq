package de.uni_mannheim.informatik.dws.desq.utils;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.Arrays;

import org.apache.hadoop.io.WritableUtils;



/**
 * Sequence class for custom comparator
 * 
 * @author Alexander Renz
 * @author Axel Rantil (axel@rantil.se)
 *
 */
public class Sequence implements Serializable {
	private byte[] sequence;
	
	
	/**
	 * Compresses an Array of integers
	 * @param seq
	 */
	public Sequence(int[] seq) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);

		try {
			// Store the length of the array so it is easier to read the array
			// later on
			WritableUtils.writeVInt(dos, seq.length);

			for (int i = 0; i < seq.length; i++) {
				WritableUtils.writeVInt(dos, seq[i]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		sequence = baos.toByteArray();
	}
	
	/**
	 * @return the compressed array of integers
	 */
	public int[] decompressSequence(){
		ByteArrayInputStream bais = new ByteArrayInputStream(sequence);
		DataInputStream dis = new DataInputStream(bais);
		int[] seq = null;

		try {
			seq = new int[WritableUtils.readVInt(dis)];

			for (int i = 0; i < seq.length; i++) {
				seq[i] = WritableUtils.readVInt(dis);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return seq;
	}
	
	private void writeObject(java.io.ObjectOutputStream out) throws IOException{
		WritableUtils.writeCompressedByteArray(out, sequence);
	}
	private void readObject(java.io.ObjectInputStream in) throws IOException{
		sequence = WritableUtils.readCompressedByteArray(in);
	}
	private void readObjectNoData() throws ObjectStreamException {
	}
	
	@Override
	public int hashCode() {
		return Arrays.hashCode(this.sequence);
	}
	
	@Override
	public boolean equals(Object o) {
		if(! (o instanceof Sequence))
			return false;
		
		final Sequence other = (Sequence) o;
		
		return Arrays.equals(this.sequence, other.sequence);
	}

	
}
