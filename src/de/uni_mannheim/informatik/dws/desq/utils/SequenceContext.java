package de.uni_mannheim.informatik.dws.desq.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.api.java.JavaSparkContext;

/**
 * API for Desq.buildHierarchy()
 * 
 * @author Axel Rantil (axel@rantil.se)
 *
 */
public class SequenceContext implements Serializable {
	
	private Hierarchy hierarchy;
	private Boolean flushed;
	
	//These fields
	private List<String> seqList;
	private StringBuilder sb;
	
	// should be replaced by something like this:
//	private JavaRDD<String> sequences;
//	private ArrayList<String> seq;
	
	public SequenceContext(){
		this.hierarchy = new Hierarchy();
		this.sb = new StringBuilder();
		this.seqList = new ArrayList<String>();
		this.flushed = true;
	}
	
	/**
	 * Adds an item to the sequence
	 * @param item to be added to the sequence
	 */
	public void addToSequence(String item){
		if (flushed){
			sb.append(item);
			flushed=false;
		} else {
			sb.append(" ");
			sb.append(item);
		}
	}
	
	/**
	 * Adds an item to the hierarchy
	 * @param item to be added to the hierarchy
	 * @return true if item was known to the hierarchy, false otherwise
	 */
	public Boolean addToHierarchy(String item){
		return hierarchy.addToHierarchy(item);
	}
	
	/**
	 * Adds a parent to the item in the hierarchy
	 * @param item child
	 * @param itemParent to be added to the hierarchy
	 * @return true if the parent was previously set for the child, false otherwise
	 */
	public Boolean addToHierarchy(String item, String itemParent){
		return hierarchy.addToHierarchy(item, itemParent);
	}
	
	/**
	 * Prepares the sequence and hierarchy for a new sequence
	 */
	public void flushSequence(){
		hierarchy.flushSequence();
		seqList.add(sb.toString());
		sb.setLength(0);
		flushed=true;
	}
	
	/**
	 * @return true if the sequence is flushed, false otherwise
	 */
	public Boolean isFlushed(){
		return flushed;
	}
	
	/**
	 * Merges the SequenceContexts for the reduce-phase
	 * @param sq2
	 */
	public void mergeSqc(SequenceContext sq2) {
		seqList.addAll(sq2.seqList);
		hierarchy.mergeHierarchies(sq2.hierarchy);
	}
	
	/**
	 * Finalizing step after the reduce-phase
	 * @param sc
	 * @return a DesqDataset with statistics
	 */
	public DesqDataset getDD(JavaSparkContext sc){
		hierarchy.topologicalOrderAndFid();
		return new DesqDataset(hierarchy, sc.parallelize(seqList), true);
	}

	//Debug output: to be removed
	public void print(int partition) {
		System.out.println("Sequence's for partition: " + partition);
		int seq=0;
		for (String s : seqList){
			System.out.println("Sequence "+ ++seq + ": "+ s);
		}
		System.out.println("Hierarchy for partition: " + partition);
		hierarchy.print();
	}

	
}

