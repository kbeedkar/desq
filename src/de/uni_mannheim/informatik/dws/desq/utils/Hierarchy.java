package de.uni_mannheim.informatik.dws.desq.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import de.uni_mannheim.informatik.dws.desq.Desq;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import scala.Serializable;
import scala.Tuple2;


public class Hierarchy implements Serializable {
	
	// Indexes for the item list
	private HashMap<Integer, Item> itemsById = new HashMap<Integer, Item>();
	private HashMap<Integer, Item> itemsByFid = new HashMap<Integer, Item>();
	private HashMap<String, Item> itemsByLabel = new HashMap<String, Item>();
	
	private HashMap<Integer, Integer> topologicalOrder = new HashMap<Integer, Integer>();

	private int maxItemId = 0;
	private boolean fidCreationCompleted = false;
	
	//buildDesqDataset-related variables
	private HashMap<Integer, Integer> seqCfreqById = new HashMap<Integer, Integer>(); //cfreq for current sequence
	private DirectedGraph<Integer> parentsGraph = new DirectedGraph<Integer>(); 
	private int seq = 0; //for dfreq
	private HashSet<Integer> propagatedItems = new HashSet<Integer>(); //set for avoiding that items are propagated twice

	
	class Item implements Serializable {
		protected int id;
		protected int fid = -1;
		protected String label;
		protected int cfreq = -1;
		protected int dfreq = -1;
		protected Item[] parents = null;
		protected Item[] children = null;

		public Item(int id, String label) {
			this.id = id;
			this.label = label;
		}
		
		public void setParents(Item[] parents) {
			this.parents = parents;
		}
		
		
		//START OF buildDesqDataset-related variables and methods
		protected int lastSeenSeq = 0;
		
		/**
		 * Recursively propagate a count up to all ancestors of an item
		 * @param seqCfreq count to be propagated up in the tree
		 */
		public void propagate(int seqCfreq) {
			cfreq += seqCfreq;
			if (lastSeenSeq<seq){
				dfreq += 1;
				lastSeenSeq=seq;
			}
			if (!(parents==null)){
				for (Item i : parents){
					if (!propagatedItems.contains(i.id)){
						propagatedItems.add(i.id);	 //temporarily store id's of items to avoid that an item gets a count twice (can occur with DAGs)
						i.propagate(seqCfreq);
					}
				}
			}
		}
		
		/**
		 * Merge two items' counts
		 * @param item2
		 */
		public void mergeItems(Item item2){
			cfreq += item2.cfreq;
			dfreq += item2.dfreq;
		}
		
		//END OF buildDesqDataset-related variables and methods
		
	}

	public Hierarchy() {
	}
	
	public int getTotalNumOfItems() {
		return itemsById.size();
	}

	
	// START OF buildDesqDataset-related methods
	
	/**
	 * Adds an item to the hierarchy
	 * @param label item to be added to the hierarchy
	 * @return false if item is new to the hierarchy, true if item was known to the hierarchy
	 */
	public Boolean addToHierarchy(String label) {
		if (!itemsByLabel.containsKey(label)){ //New item
			int id = label.hashCode();
			Item item = new Item(id, label);
			
			item.cfreq = 0;
			seqCfreqById.put(id, 1);
			
			item.dfreq = 1;
			item.lastSeenSeq=seq;
			
			itemsByLabel.put(label, item);
			itemsById.put(id, item);
			parentsGraph.addNode(id);
			
			maxItemId = Math.max(maxItemId, id);
			return false;
		} else { //Known item
			Item item = itemsByLabel.get(label);
			
			if (seqCfreqById.containsKey(item.id)){ //Seen in current sequence
				seqCfreqById.put(item.id, seqCfreqById.get(item.id)+1);
			} else { //New for this seq
				seqCfreqById.put(item.id, 1);
			}
			
			if (item.lastSeenSeq<seq){
				++item.dfreq;
				item.lastSeenSeq = seq;
			}
			return true;
		}
	}
	
	/**
	 * Adds a (known or new) parent to an item and the hierarchy
	 * @param label item
	 * @param parentLabel items' parent
	 * @return false if parent was not known or set for the item, true if parent was known and set for the item
	 */
	public Boolean addToHierarchy(String label, String parentLabel) {
		
		//This shouldn't be necessary (developer should already have added the child before calling this method)
		if (!itemsByLabel.containsKey(label)){ 
			addToHierarchy(label);
		}
		
		Boolean knownParent = itemsByLabel.containsKey(parentLabel);
		
		if (!knownParent){				//Parent not known
			Item parentItem = new Item(parentLabel.hashCode(), parentLabel);
			
			parentItem.cfreq = 0; 		//Count will be propagated later
			
			parentItem.dfreq = 1;
			parentItem.lastSeenSeq=seq;
			
			itemsByLabel.put(parentLabel, parentItem);
			itemsById.put(parentItem.id, parentItem);
			parentsGraph.addNode(parentItem.id);
			
			maxItemId = Math.max(maxItemId, parentItem.id);
		} 
		
		Item child = itemsByLabel.get(label);
		Item parent = itemsByLabel.get(parentLabel);
		
		Boolean parentSet = false;
		
		if (!(child.parents==null)){ 	//Child has parents, check if parent is there
			for (Item parentsOfChild : child.parents){
				parentSet = ((parentsOfChild.id == parent.id)) ? true: false;
			}
		}
		
		if (knownParent && parentSet){	// Parent is known and set as parent for this label
			return true;
		} else { 						// Parent (now) exists but is not set
			
			if (child.parents==null){ 	// If child has no parents, set parent argument as parents
				child.setParents(new Item[]{parent});
				parentsGraph.addEdge(parent.id, child.id);
			} else { 					// If child has parent, copy previous array and add new parent
				Item[] oldParents = child.parents;
				Item[] parentList = new Item[oldParents.length+1];
				for (int i=0; i<oldParents.length; i++){
					parentList[i] = oldParents[i];
				}
				parentList[parentList.length] = parent;
				parentsGraph.addEdge(parent.id, child.id);
				
				child.setParents(parentList);
			} 
			return false;
		} 
	}
	
	/**
	 * Finishes this sequence and prepares for a new one
	 */
	public void flushSequence() {
		for (int i: seqCfreqById.keySet()){
			itemsById.get(i).propagate(seqCfreqById.get(i));
			propagatedItems.clear();
		}
		seqCfreqById.clear();
		++seq;
	}
	
	/**
	 * Merges hierarchies for the reduce part
	 * @param hierarchy2
	 */
	public void mergeHierarchies(Hierarchy hierarchy2) {
		
		maxItemId = Math.max(maxItemId, hierarchy2.maxItemId);
		HashMap<Integer,Item> newItems = new HashMap<Integer,Item>();
		
		// Merge existing items and assign references to items new for this hierarchy
		for (int i2: hierarchy2.itemsById.keySet()){ 
			if (itemsById.containsKey(i2)){
				itemsById.get(i2).mergeItems(hierarchy2.itemsById.get(i2));
			} else {
				Item item2 = hierarchy2.itemsById.get(i2);
				newItems.put(i2, item2);
				itemsById.put(i2, item2);
				itemsByLabel.put(item2.label, item2);
			}
		}
		
		// Make sure the new items reference to the items of this hierarchy
		for (Item newItem : newItems.values()){ 
			if (!(newItem.parents==null)){
				Item[] newParents = new Item[newItem.parents.length];
				for (int i=0; i<newParents.length; i++){
					newParents[i] = itemsById.get(newItem.parents[i].id);
				}
				newItem.setParents(newParents);
			}
		}
		
		parentsGraph.addAllEdgesAndNodes(hierarchy2.parentsGraph);
	}
	
	/**
	 * Orders and generate fid's of the reduced hierarchy
	 */
	public void topologicalOrderAndFid(){
		// Check for cycles in the hierarchy
		List<Integer> sorted = null;
		try {
			sorted = TopologicalSort.sort(parentsGraph);
		} catch (IllegalArgumentException e) {
			System.out.println("The given hierarchy contains a cycle. Exiting...");
			System.exit(1);
		}
		// If there are no cycles, store the topological order
		for (int i = 0; i < sorted.size(); i++) {
			topologicalOrder.put(sorted.get(i), i);
		}
		
		//buildDesqDataset-related variables aren't needed anymore
		parentsGraph = null;
		seqCfreqById.clear();
		
		generateFid();

	}
	
	// END OF buildDesqDataset-related methods
	
	
	
	/**
	 * Loads an instance without statistics from a CSV-file 
	 * label<tab>parent1,parent2...
	 * 
	 * @param sc
	 * @param filePathCSV
	 */
	public void loadCSV(JavaSparkContext sc, String filePathCSV) {
		
		JavaRDD<String> strRDD = sc.textFile(filePathCSV);

		final HashMap<String, Item> tempMap = new HashMap<String, Item>();
		
		List<String> strList = strRDD.collect();
		int id = 0;
		
		for (String s : strList) {
			String[] splits = s.split("\t");
			String itemLabel = splits[0].trim();
			Item item = new Item(++id, itemLabel);
			tempMap.put(itemLabel, item);
		}
		strRDD = sc.parallelize(strList);
		
		JavaRDD<Row> rowRDD = strRDD.map(
			new Function<String, Row>() {
				public Row call(String tabSeparatedString) throws Exception {
					String[] columns = tabSeparatedString.split("\t");
					String itemLabel = columns[0];
					long[] parentArr = null;
					if (columns.length>1){ // has parent
						String [] parentArray = columns[1].trim().split(",");
						parentArr = new long[parentArray.length];
						for (int i=0; i<parentArray.length; i++){
							parentArr[i] = (long) tempMap.get(parentArray[i].trim()).id;
						}
					}
					return RowFactory.create((long)(tempMap.get(itemLabel).id), itemLabel, parentArr);
				}
			}); 
		
		List<StructField> fields = new ArrayList<StructField>();
		fields.add(DataTypes.createStructField("id", DataTypes.LongType, false));
		fields.add(DataTypes.createStructField("label", DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("parents", DataTypes.createArrayType(DataTypes.LongType), true));
		
		StructType schema = DataTypes.createStructType(fields);
		
		SQLContext sqlContext = new org.apache.spark.sql.SQLContext(sc);
		DataFrame hierDf = sqlContext.createDataFrame(rowRDD, schema);
		loadInstance(hierDf, false);
		tempMap.clear();
	}
	
	/**
	 * Loads an instance from a JSON-file either with or without statistics
	 * 
	 * @param sc
	 * @param fileName
	 * @param withStatistics
	 */
	public void loadJSON(JavaSparkContext sc, String fileName, Boolean withStatistics) {

		// Read JSON dictionary / hierarchy file
		SQLContext sqlContext = new org.apache.spark.sql.SQLContext(sc);
		DataFrame hierDf = sqlContext.read().json(fileName);
		loadInstance(hierDf, withStatistics);
		
	}
	
	/**
	 * Loads an instance from a DataFrame either with or without statistics, method is used by both loadCSV and loadJSON
	 * 
	 * @param hierDf
	 * @param withStatistics
	 */
	private void loadInstance(DataFrame hierDf, Boolean withStatistics){
		// Temporary store for the parents (using id's) until we have created
		// all objects
		HashMap<Integer, List<Long>> tempParents = new HashMap<Integer, List<Long>>();
		DirectedGraph<Integer> parentsGraph = new DirectedGraph<Integer>();

		Row[] hierarchyElements = hierDf.collect();

		final int ID = hierarchyElements[0].fieldIndex("id");
		final int LABEL = hierarchyElements[0].fieldIndex("label");
		final int PARENTS = hierarchyElements[0].fieldIndex("parents");
		final int CFREQ;
		final int DFREQ;
		
		if (withStatistics){
			CFREQ = hierarchyElements[0].fieldIndex("cfreq");
			DFREQ = hierarchyElements[0].fieldIndex("dfreq");
		} else {CFREQ = 0; DFREQ = 0;} 
		
		// One row is one hierarchy element
		for (Row row : hierarchyElements) {
			int itemId = (int) row.getLong(ID);
			String itemLabel = row.getString(LABEL);
			List<Long> itemParents = null;
			if (!row.isNullAt(PARENTS))
				itemParents = row.getList(PARENTS);

			maxItemId = Math.max(maxItemId, itemId);

			// Build hierarchy graph for topological sort (later on)
			parentsGraph.addNode(itemId);

			if (itemParents != null && itemParents.size() > 0) {
				tempParents.put(itemId, itemParents);
				for (Long parentLong : itemParents) {
					Integer parentId = parentLong.intValue();

					parentsGraph.addNode(parentId);
					parentsGraph.addEdge(parentId, itemId);
				}
			}

			// Create the Item object and add it to the item map
			Item item = new Item(itemId, itemLabel);
			if (withStatistics) {
				item.cfreq = (int) row.getLong(CFREQ);
				item.dfreq = (int) row.getLong(DFREQ);
			}
			itemsById.put(itemId, item);
			itemsByLabel.put(itemLabel, item);

		}

		// As have an Item object for every item now, we can add references to
		// these objects as parents lists
		for (Map.Entry<Integer, Item> entry : itemsById.entrySet()) {
			if (tempParents.containsKey(entry.getKey())) {
				List<Long> itemParents = tempParents.get(entry.getKey());
				Item[] parents = new Item[itemParents.size()];
				for (int i = 0; i < itemParents.size(); i++) {
					parents[i] = itemsById.get(itemParents.get(i).intValue());
				}
				entry.getValue().setParents(parents);
			}
		}

		// Check for cycles in the hierarchy
		List<Integer> sorted = null;
		try {
			sorted = TopologicalSort.sort(parentsGraph);
		} catch (IllegalArgumentException e) {
			System.out.println("The given hierarchy contains at least one cycle. Exiting...");
			System.exit(1);
		}
		// If there are no cycles, store the topological order
		for (int i = 0; i < sorted.size(); i++) {
			topologicalOrder.put(sorted.get(i), i);
		}
		
		if (withStatistics){
			generateFid();
		}
		
		// Compute children form each item
		HashMap<Integer, ObjectArrayList<Item>> tempChildrenById = new HashMap<Integer, ObjectArrayList<Item>>();
		for(int id : itemsById.keySet()) {
			tempChildrenById.put(id, new ObjectArrayList<Item>());
		}
		
		// Collect children for each item
		for(int id : itemsById.keySet()) {
			if(itemsById.get(id).parents != null && itemsById.get(id).parents.length > 0) {
				for(Item parent : itemsById.get(id).parents) {
					tempChildrenById.get(parent.id).add(itemsById.get(id));
				}
			}
		}
		
		// Store children
		for(int id : itemsById.keySet()) {
			if(tempChildrenById.get(id).size() > 0) {
				itemsById.get(id).children = new Item[tempChildrenById.get(id).size()];
				tempChildrenById.get(id).toArray(itemsById.get(id).children);
			}
		}
	}
	
	
	/**
	 * Saves a hierarchy in JSON-format without statistics
	 * 
	 * @param sc
	 * @param savePath path to where the JSON-file should be saved
	 */
	public void save(JavaSparkContext sc, String savePath){
			
		dataFrameBuilder(sc, false).coalesce(1).write().mode(SaveMode.Overwrite).json(savePath);
			
	}

	/**
	 * Saves a hierarchy in JSON-format either with or without statistics
	 * 
	 * @param sc
	 * @param savePath path to where the JSON-file should be saved
	 * @param withStatistics true if hierarchy should be saved with statistics, false otherwise
	 */
	public void save(JavaSparkContext sc, String savePath, Boolean withStatistics){
		
		dataFrameBuilder(sc, withStatistics).coalesce(1).write().mode(SaveMode.Overwrite).json(savePath);
		
	}
	
	/**
	 * Builds a DataFrame from the instance used by save().
	 * 
	 * @param sc
	 * @param withStatistics true if DataFrame should be built with statistics, false otherwise
	 * @return
	 */
	private DataFrame dataFrameBuilder(JavaSparkContext sc, Boolean withStatistics){
		
		if (withStatistics && !fidCreationCompleted){
			System.out.println("Your hierarchy hasn't computed any statistics! Load a DesqDataset first");
		}
		
		List<Row> row = new ArrayList<Row>();
		for (int i : itemsById.keySet()){
			Item item = itemsById.get(i);
			int id = item.id;
			String label = item.label;
			long[] parentArray = null;
			if (item.parents != null){
				parentArray = new long[item.parents.length];
				for (int j=0; j<item.parents.length; j++){
					parentArray[j] = (long) item.parents[j].id;
				}
			}
			if (withStatistics){
				int cfreq = item.cfreq;
				int dfreq = item.dfreq;
				row.add(RowFactory.create(id, cfreq, dfreq, label, parentArray));
			} else {
				row.add(RowFactory.create(id, null, null, label, parentArray));
			}
		}
		
		JavaRDD<Row> rowRDD = sc.parallelize(row);
		
		SQLContext sqlContext = new org.apache.spark.sql.SQLContext(sc);
		return sqlContext.createDataFrame(rowRDD, createHierSchema());
	}
	
	private StructType createHierSchema(){
		List<StructField> fields = new ArrayList<StructField>();
		fields.add(DataTypes.createStructField("id", DataTypes.IntegerType, false));
		fields.add(DataTypes.createStructField("cfreq", DataTypes.IntegerType, true));
		fields.add(DataTypes.createStructField("dfreq", DataTypes.IntegerType, true));
		fields.add(DataTypes.createStructField("label", DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("parents", DataTypes.createArrayType(DataTypes.LongType), true));
		return DataTypes.createStructType(fields);
	}
	
	
	/**
	 * Generate f.id's (frequency-based item id's), cFreq and dFreq
	 * 
	 * @param sequences
	 */
	public void createFIDsFromSequences(JavaRDD<Sequence> sequences) {

		/**
		 * 
		 * Count cFreq
		 * 
		 * 
		 */

		/** Count items in sequences */
		JavaRDD<Integer> sequenceItems = sequences.flatMap(new FlatMapFunction<Sequence, Integer>() {
			@Override
			public Iterable<Integer> call(Sequence seq) throws Exception {
				// Note: not extremely efficient - better way?
				return (Iterable<Integer>) Arrays.asList(ArrayUtils.toObject(Desq.decompressSequence(seq)));
			}
		});
		JavaPairRDD<Integer, Integer> itemPairs = sequenceItems
				.mapToPair(new PairFunction<Integer, Integer, Integer>() {
					public Tuple2<Integer, Integer> call(Integer item) {
						return new Tuple2<Integer, Integer>(item, 1);
					}
				});
		JavaPairRDD<Integer, Integer> itemCounts = itemPairs.reduceByKey(new Function2<Integer, Integer, Integer>() {
			@Override
			public Integer call(Integer a, Integer b) throws Exception {
				return a + b;
			}
		});

		// HashMap<Integer, Integer> itemFrequencies = new HashMap<Integer,
		// Integer>();

		/** Add counters for ancestor items */
		// itemFrequencies.clear(); // clear map in case this method (f.id
		// creation) is run more than once
		List<Tuple2<Integer, Integer>> frequenciesCounted = itemCounts.collect();
		for (Tuple2<Integer, Integer> itemCount : frequenciesCounted) {
			int itemId = itemCount._1;
			int itemFreq = itemCount._2;

			Item item = itemsById.get(itemId);

			if (item.cfreq == -1) {
				item.cfreq = 0;
			}

			item.cfreq = item.cfreq + itemFreq;

			// Increase freq for ancestors, climbing up the hierarchy
			// Note: this uses only the first parent, so there is no support for
			// DAGs here.
			while (item.parents != null && item.parents.length > 0) {
				item = item.parents[0];
				if (item.cfreq == -1) {
					item.cfreq = 0;
				}
				item.cfreq = item.cfreq + itemFreq;
			}
		}

		/**
		 * 
		 * Count dFreq
		 * 
		 * 
		 */

		/** Count items in sequences */
		JavaRDD<Integer> sequenceItemsDfreq = sequences.flatMap(new FlatMapFunction<Sequence, Integer>() {
			@Override
			public Iterable<Integer> call(Sequence seq) throws Exception {
				// Note: not extremely efficient - better way?
				Set<Integer> items = new HashSet<Integer>();
				for (int item : Desq.decompressSequence(seq))
					items.add(item);
				return items;
			}
		});
		JavaPairRDD<Integer, Integer> itemPairsDfreq = sequenceItemsDfreq
				.mapToPair(new PairFunction<Integer, Integer, Integer>() {
					public Tuple2<Integer, Integer> call(Integer item) {
						return new Tuple2<Integer, Integer>(item, 1);
					}
				});
		JavaPairRDD<Integer, Integer> itemCountsDfreq = itemPairsDfreq
				.reduceByKey(new Function2<Integer, Integer, Integer>() {
					@Override
					public Integer call(Integer a, Integer b) throws Exception {
						return a + b;
					}
				});

		// Add counters for ancestor items
		List<Tuple2<Integer, Integer>> frequenciesCountedDfreq = itemCountsDfreq.collect();
		for (Tuple2<Integer, Integer> itemCount : frequenciesCountedDfreq) {
			int itemId = itemCount._1;
			int itemFreq = itemCount._2;

			Item item = itemsById.get(itemId);

			if (item.dfreq == -1) {
				item.dfreq = 0;
			}

			item.dfreq = item.dfreq + itemFreq;

			// Increase freq for ancestors, climbing up the hierarchy
			// Note: this uses only the first parent, so there is no support for
			// DAGs here.
			while (item.parents != null && item.parents.length > 0) {
				item = item.parents[0];
				if (item.dfreq == -1) {
					item.dfreq = 0;
				}
				item.dfreq = item.dfreq + itemFreq;
			}

		}
		// done counting
		generateFid();
	}
	
	
	/**
	 * Assumes the cfs and dfs is computed and generates fid's
	 * 
	 */
	private void generateFid(){

		Set<Integer> itemSet = itemsById.keySet();

		Integer[] terms = itemSet.toArray(new Integer[itemSet.size()]);

		/** Sort items */
		// first: by ascending topological order
		Arrays.sort(terms, new Comparator<Integer>() {
			@Override
			public int compare(Integer t, Integer u) {
				// 'smaller' order first

				// if t is not in the hierarchy, sort it to the end
				if (!topologicalOrder.containsKey(t)) {
					return 1;
				}
				// if u is not in the hierarchy, sort it to the end
				else if (!topologicalOrder.containsKey(u)) {
					return -1;
				} else
					return topologicalOrder.get(t) - topologicalOrder.get(u);
			}
		});

		// second: by descending cfs
		Arrays.sort(terms, new Comparator<Integer>() {
			@Override
			public int compare(Integer t, Integer u) {
				// 'larger' cfs first
				return itemsById.get(u).cfreq - itemsById.get(t).cfreq;
			}
		});
		int i = 1;
		// Store the fid's
		for (int term : terms) {
			// i+1 is the fid
			// term

			// add the fid to corresponding object
			itemsById.get(term).fid = i;

			// add the object pointer to the byFid map
			itemsByFid.put(i, itemsById.get(term));
			i = i + 1;
		}

		this.fidCreationCompleted = true;
	}

	
	public void loadOld(String pathHier) throws NumberFormatException, IOException {

		DirectedGraph<Integer> parentsGraph = new DirectedGraph<Integer>();

		// Temporary store for the parents (using id's) until we have created
		// all objects
		HashMap<Integer, List<Long>> tempParents = new HashMap<Integer, List<Long>>();
		
		BufferedReader br = new BufferedReader(new FileReader(pathHier));
		String line = null;
		
		while ((line = br.readLine()) != null) {
			String[] splits = line.split("\t");
			String itemLabel = splits[0].trim();
			int cfs = Integer.parseInt(splits[1].trim());
			int dfs = Integer.parseInt(splits[2].trim());
			int itemId = Integer.parseInt(splits[3].trim());
			List<Long> itemParents = null;
			//Only one parent right now
			if (!splits[4].isEmpty() && !(Integer.parseInt(splits[4].trim())== 0)) {
				itemParents.add((long) Integer.parseInt(splits[4].trim()));
			}

			maxItemId = Math.max(maxItemId, itemId);

			// Build hierarchy graph for topological sort (later on)
			parentsGraph.addNode(itemId);

			if (itemParents != null && itemParents.size() > 0) {
				tempParents.put(itemId, itemParents);
				for (Long parentLong : itemParents) {
					Integer parentId = parentLong.intValue();

					parentsGraph.addNode(parentId);
					parentsGraph.addEdge(parentId, itemId);
				}
			}

			// Create the Item object and add it to the item map
			Item item = new Item(itemId, itemLabel);
			item.cfreq = cfs;
			item.dfreq = dfs;
			itemsById.put(itemId, item);
			itemsByLabel.put(itemLabel, item);
		}


		// As have an Item object for every item now, we can add references
		// to
		// these objects as parents lists
		for (Map.Entry<Integer, Item> entry : itemsById.entrySet()) {
			if (tempParents.containsKey(entry.getKey())) {
				List<Long> itemParents = tempParents.get(entry.getKey());
				Item[] parents = new Item[itemParents.size()];
				for (int i = 0; i < itemParents.size(); i++) {
					parents[i] = itemsById.get(itemParents.get(i).intValue());
				}
				entry.getValue().setParents(parents);
			}
		}

		// Check for cycles in the hierarchy
		List<Integer> sorted = null;
		try {
			sorted = TopologicalSort.sort(parentsGraph);
		} catch (IllegalArgumentException e) {
			System.out.println("The given hierarchy contains a cycle. Exiting...");
			System.exit(1);
		}
		// If there are no cycles, store the topological order
		for (int i = 0; i < sorted.size(); i++) {
			topologicalOrder.put(sorted.get(i), i);
		}
		generateFid();
	}
	

	/**
	 * Create the ancestors data structure
	 * 
	 * Careful! this.parents contains id's this.ancestorsList and
	 * this.ancestorsListsPositions work with fid's
	 */
	/*
	 * we won't be doing this anymore public void createAncestorsList() {
	 * 
	 * IntArrayList tempParentsList = new IntArrayList(); int currentPosition =
	 * 0; HashMap<Integer, Integer> tempParentsListPositions = new
	 * HashMap<Integer, Integer>();
	 * 
	 * for(int itemFid=1; itemFid<fidToId.length; itemFid++) { int itemId =
	 * fidToId[itemFid];
	 * 
	 * tempParentsListPositions.put(itemFid, currentPosition);
	 * 
	 * if(parents.containsKey(itemId) && parents.get(itemId).size() > 0) {
	 * for(int i=0; i<parents.get(itemId).size(); i++) { int parentId =
	 * parents.get(itemId).get(i); tempParentsList.add(idToFid.get(parentId));
	 * currentPosition++; } }
	 * 
	 * }
	 * 
	 * 
	 * ancestorsList = tempParentsList.toArray(new int[tempParentsList.size()]);
	 * ancestorsListPositions = new int[maxItemId+2]; // 0 not used + dummy item
	 * at the end
	 * 
	 * for(Entry<Integer, Integer> entry : tempParentsListPositions.entrySet())
	 * { ancestorsListPositions[entry.getKey()] = entry.getValue(); }
	 * 
	 * // Add dummy item at the end of the positions list to make access easier
	 * for the last item ancestorsListPositions[ancestorsListPositions.length -
	 * 1] = ancestorsList.length;
	 * 
	 * 
	 * // TODO: will we still require this? (with the fid's, it is not necessary
	 * anymore, right?) --Alex // Test the order: for each item, all parents
	 * need to have a lower id for(int i=1; i<fidToId.length; i++) { // check
	 * all parents for(int pos=ancestorsListPositions[i];
	 * pos<ancestorsListPositions[i+1]; pos++) { if(ancestorsList[pos] > i) {
	 * System.out.println("ERROR: Item " + i + " has parent " +
	 * ancestorsList[pos] + ", which as a higher ID."); System.exit(1); } } } }
	 */

	public void clear() {
		itemsById.clear();
		itemsByFid.clear();
		topologicalOrder.clear();
		fidCreationCompleted = false;
	}

	// Debug output
	public void print() {

		for (Map.Entry<Integer, Item> itemEntry : itemsById.entrySet()) {
			Item item = itemEntry.getValue();

			StringBuilder sb = new StringBuilder();
			sb.append("{fid:" + item.fid);
			sb.append(", id:" + item.id);
			sb.append(", label:" + item.label);
			
			StringBuilder parentsSb = new StringBuilder();
			// Collect parents / ancestors
			if (item.parents != null) {
				for (Item parent : item.parents) {
					parentsSb.append(" ");
					parentsSb.append(parent.id);
				}
			}
			
			StringBuilder childrenSb = new StringBuilder();
			// Collect parents / ancestors
			if (item.children != null) {
				for (Item child : item.children) {
					childrenSb.append(" ");
					childrenSb.append(child.id);
				}
			}

			if (parentsSb.length() > 0)
				sb.append(", parents-ids:" + parentsSb.toString());
			
			if (childrenSb.length() > 0)
				sb.append(", children-ids:" + childrenSb.toString());


			sb.append(", dfreq: " + item.dfreq + ", cfreq: " + item.cfreq);

			sb.append("}");

			System.out.println(sb.toString());
		}
	}

	/*******
	 * New methods for Desq
	 * 
	 */

	// Note: this works with fids!
	public boolean isParent(int parentFid, int childFid) {
		if (itemsByFid.get(childFid).parents == null)
			return false;
		for (Item parent : itemsByFid.get(childFid).parents) {
			if (parent.fid == parentFid) {
				return true;
			}
		}
		return false;
	}

	public boolean hasParent(int itemFid) {
		return itemsByFid.get(itemFid).parents != null && itemsByFid.get(itemFid).parents.length > 0;
	}

	public int getFirstParent(int itemFid) {
		return itemsByFid.get(itemFid).parents[0].fid;
	}
	
	public int[] getParents(int itemFid) {
		int[] parentFids = new int[itemsByFid.get(itemFid).parents.length];
		for(int i=0; i<parentFids.length; i++) {
			parentFids[i] = itemsByFid.get(itemFid).parents[i].fid;
		}
		return parentFids;
	}

	// Note: flist works with fids!
	public int[] getFlist() {

		if (!this.fidCreationCompleted)
			System.out.println("CAREFUL! Corpus was not scanned yet, so fList is empty.");

		int[] flist = new int[itemsByFid.size() + 1];
		for (int i = 0; i < itemsByFid.size(); i++) {
			flist[i + 1] = itemsByFid.get(i + 1).cfreq;
		}

		return flist;
	}

	public int getFid(int id) {
		return itemsById.get(id).fid;
	}

	public int getFid(String label) {
		return itemsByLabel.get(label).fid;
	}

	public int getId(int fid) {
		return itemsByFid.get(fid).id;
		// return fidToId[fid];
	}

	public int getId(String label) {
		return itemsByLabel.get(label).id;
	}

	public String[] getLabelA(int id[]) {
		String[] Str = new String[id.length];
		for (int i = 0; i < id.length; i++) {
			Str[i] = itemsById.get(id[i]).label;
		}
		return Str;
	}

	public Sequence translateToFids(Sequence seq) {
		int[] sequence = Desq.decompressSequence(seq);
		for (int i = 0; i < sequence.length; i++)
			sequence[i] = getFid(sequence[i]);
		return Desq.compressSequence(sequence);
	}

	public Sequence translateToIds(Sequence seq) {
		int[] sequence = Desq.decompressSequence(seq);
		for (int i = 0; i < sequence.length; i++)
			sequence[i] = getId(sequence[i]);
		return Desq.compressSequence(sequence);
	}
	
	public int[] getChildrenFids(int itemFid) {
		Item item = itemsByFid.get(itemFid);
		
		if(item.children == null)
			return new int[0];
		
		int[] children = new int[item.children.length];
		for(int i=0; i<children.length; i++) {
			children[i] = item.children[i].fid;
		}
		return children;
	}
	
	public int[] getDescendantsFids(int itemFid) {
		IntOpenHashSet desc = new IntOpenHashSet();
		IntArrayList stack = new IntArrayList();
		stack.add(itemFid);
		int top = 0;
		while(top < stack.size()) {
			int descId = stack.getInt(top);
			if(!desc.contains(descId)) {
				desc.add(descId);
				for(int child : getChildrenFids(descId)) {
					stack.add(child);
				}
			}
			top++;
		}
		stack = null;
		return desc.toIntArray();
	}


}
