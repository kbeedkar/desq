package de.uni_mannheim.informatik.dws.desq.utils;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import de.uni_mannheim.informatik.dws.desq.Desq;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class for the mining input
 * 
 * @author Axel Rantil (axel@rantil.se)
 *
 */

public class DesqDataset implements Serializable {

	public enum Status {
		LABEL, ID, FID
	}

	private Hierarchy hierarchy;
	private JavaRDD<Sequence> seqData;
	private JavaRDD<String> strData;
	private Status status;

	public DesqDataset(Hierarchy hierarchy, JavaRDD<String> strData, Boolean statsComputed){
		this.hierarchy=hierarchy;
		this.strData=strData;
		this.status=Status.LABEL;
		if (!statsComputed){
			JavaRDD<Sequence> temp = labelToId(strData); 
			hierarchy.createFIDsFromSequences(temp);
		}
	}
	
	//For DesqDataset.load
	public DesqDataset(){
	};
	
	/**
	 * Recodes the sequences
	 * @param str 
	 * 				input of desired status: "label", "id" or "fid"
	 */
	public void encode(String str){
		str = str.trim().toLowerCase();
		Status desiredStatus = null;
		if (str.equals("label")){
			desiredStatus = Status.LABEL;
		} else if (str.equals("id")){
			desiredStatus = Status.ID;
		} else if (str.equals("fid")){
			desiredStatus = Status.FID;
		} else {
			System.out.println("Wrong input! Please input \"label\", \"id\" or \"fid\"");
			return;
		}
		if (status.equals(desiredStatus))
			System.out.println("Your dataset is already encoded in your desired status!");
		switch (status){
		case LABEL:
			if (desiredStatus.equals(Status.ID)){
				seqData = labelToId(strData);
				strData = null;
				status = Status.ID;
			} else if (desiredStatus.equals(Status.FID)){
				JavaRDD<Sequence> temp = labelToId(strData);
				seqData = idToFid(temp);
				strData = null;
				status = Status.FID;
			}	
			break;
		case ID:
			if (desiredStatus.equals(Status.LABEL)){
				strData = idToLabel(seqData);
				seqData = null;
				status=Status.LABEL;
			} else if (desiredStatus.equals(Status.FID)){
				seqData = idToFid(seqData);
				status=Status.FID;
			}
			break;
		case FID:
			if (desiredStatus.equals(Status.ID)){
				seqData = fidToId(seqData);
				status=Status.ID;
			} else if (desiredStatus.equals(Status.LABEL)){
				JavaRDD<Sequence> temp = fidToId(seqData);
				strData = idToLabel(temp);
				seqData = null;
				status=Status.LABEL;
			}
			break;
		}
	}
	
	/**
	 * Saves the DesqDataset and returns a new DesqDataset instance loaded from the saved files
	 * 
	 * @param sc
	 * @param 
	 * 				filePath desired path of the file containing pointers to the actual contents of the DesqDataset
	 * @return DesqDataset instance loaded from the saved DesqDataset
	 */
	public DesqDataset save(JavaSparkContext sc, String filePath){
		String fileSeparator = System.getProperty("file.separator");

		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyMMdd'-'HHmmss");
		String timestamp = format.format(now);
		String dirName = "desq-dataset"+fileSeparator+timestamp;
		
		File dir = new File(dirName);
		dir.mkdirs();
		
		String hierSavePath = dirName+fileSeparator+"hierarchy";
		String seqSavePath = dirName+fileSeparator+"sequence";
		
		hierarchy.save(sc, hierSavePath, true);
		
		switch (status){
		case LABEL:
			strData.coalesce(1).saveAsTextFile(seqSavePath);
			break;
		case ID:
			seqData.coalesce(1).saveAsObjectFile(seqSavePath);
			break;
		case FID:
			seqData.coalesce(1).saveAsObjectFile(seqSavePath);
			break;
		}

		final Charset ENCODING = StandardCharsets.UTF_8;
		
		Path path = Paths.get(filePath);
		
		List<String> lines = new ArrayList<String>();
		lines.add(status.toString());
		lines.add(hierSavePath);
		lines.add(seqSavePath);
		
		try {
			Files.write(path, lines, ENCODING);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		DesqDataset dd = new DesqDataset();
		dd.load(sc, filePath);
		return dd;
	}
	
	
	
	/**
	 * Loads a DesqDataset saved with DesqDataset.save()-method
	 * 
	 * @param sc
	 * @param filePath 
	 * 				the path given as an argument for the save()-method
	 */
	public void load(JavaSparkContext sc, String filePath) {
		
		final Charset ENCODING = StandardCharsets.UTF_8;
		
		Path path = Paths.get(filePath);
		
		List<String> lines = null;
		
		try {
			lines = Files.readAllLines(path, ENCODING);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		//Get contents
		
		String strStatus = lines.get(0);
		String hierPath = lines.get(1);
		String seqPath = lines.get(2);
		
		status=Status.valueOf(strStatus);
		
		//Loading hierarchy with stats
		try {
			hierarchy = new Hierarchy();
			hierarchy.loadJSON(sc, hierPath, true);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		//Loading sequence depending on status
		switch (status){
		case LABEL:
			strData = sc.textFile(seqPath);
			break;
		case ID:
			seqData = sc.objectFile(seqPath);
			break;
		case FID:
			seqData = sc.objectFile(seqPath);
			break;
		}
		
	}

	private JavaRDD<Sequence> labelToId(JavaRDD<String> in) {
		JavaRDD<Sequence> out = in.map(new Function<String, Sequence>() {
			@Override
			public Sequence call(String str) throws Exception {
				String[] stra = str.split(" ");
				int[] inta = new int[stra.length];
				for (int i = 0; i < stra.length; i++) {
					inta[i] = hierarchy.getId(stra[i]);
				}
				return Desq.compressSequence(inta);
			}
		});
		return out;
	}

	//Compressed before and after
	private JavaRDD<Sequence> idToFid(JavaRDD<Sequence> in){
		JavaRDD<Sequence> out = in.map(new Function<Sequence, Sequence>() { 
			@Override
			public Sequence call(Sequence seq) {
				return hierarchy.translateToFids(seq);
			}
		});
		return out;
	}


	//Compressed before and after
	private JavaRDD<Sequence> fidToId(JavaRDD<Sequence> in){
		JavaRDD<Sequence> out = in.map(new Function<Sequence, Sequence>() {
			@Override
			public Sequence call(Sequence seq) {
				return hierarchy.translateToIds(seq);
			}
		});
		return out;
	}

	private JavaRDD<String> idToLabel(JavaRDD<Sequence> in){
		JavaRDD<String> out = in.map(new Function<Sequence, String>() {
			@Override
			public String call(Sequence seq) throws Exception {
				return Desq.labelToString(hierarchy.getLabelA((Desq.decompressSequence(seq))));
			}
		});
		return out;
	}

	/**
	 * @return the hierarchy for this dataset
	 */
	public Hierarchy getHierarchy() {
		return hierarchy;
	}

	/**
	 * @return the encoded sequences
	 */
	public JavaRDD<Sequence> getSequences() {
		return seqData;
	}

	/**
	 * @return true if this dataset is prepared for mining, false otherwise
	 */
	public Boolean isPrepared() {
		return status.equals(Status.FID);
	}
	
	/**
	 * Prepares the dataset for mining
	 */
	public void prepare() {
		encode("fid");
	}

	/**
	 * @return the current encoding (status) of the dataset
	 */
	public String getStatus(){
		return status.toString();
	}
	
	//Debug output: to be removed
	public void print(){
		List<String> testlist = strData.collect();
		for (String s: testlist){
			System.out.println(s);
		}
		System.out.println("Hierarchy:");
		hierarchy.print();
	}
	//Debug output: to be removed
	public void printID(){
		List<Sequence> testlist = seqData.collect();
		for (Sequence s: testlist){
			System.out.println(Desq.intsToString(Desq.decompressSequence(s)));
		}
		System.out.println("Hierarchy:");
		hierarchy.print();
	}
	
	//Constructor for old dataset
	public DesqDataset(Hierarchy hierarchy, JavaRDD<Sequence> seqData){
		this.hierarchy=hierarchy;
		this.seqData=seqData;
		this.status=Status.FID;
	}

	

}
