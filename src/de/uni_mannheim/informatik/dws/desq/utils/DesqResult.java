package de.uni_mannheim.informatik.dws.desq.utils;



import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import de.uni_mannheim.informatik.dws.desq.Desq;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import scala.Tuple2;

/**
 * Class for the result of the Desq.mine()
 * 
 * @author Axel Rantil 
 *
 */
public class DesqResult implements Serializable{

	private static Hierarchy hierarchy;
	private JavaRDD<Tuple2<Sequence, Integer>> patterns;
	private JavaRDD<Tuple2<String,Integer>> decodedPatterns;
	private Boolean decoded;

	public DesqResult(Hierarchy hierarchy, JavaRDD<Tuple2<Sequence, Integer>> patterns){
		DesqResult.hierarchy=hierarchy;
		this.patterns=patterns;
		this.decoded=false;
	}
	
	/**
	 * Constructor for .load()
	 */
	public DesqResult() {
	}

	/**
	 * Decodes the patterns from the mining either to label or id
	 * @param desiredDecode input "label" or "id"
	 * @return RDD of strings of labels or integers depending on input argument
	 */
	public JavaRDD<Tuple2<String, Integer>> decode(String desiredDecode){
		String desiredD = desiredDecode.trim().toLowerCase();
		if (desiredD.equals("label")){
			decodedPatterns = patterns.map(new ToString());
			decoded = true;
		} else if (desiredD.equals("id")){
			decodedPatterns = patterns.map(new ToID());
			decoded = true;
		} else {
			System.out.println("Please input \"label\" or \"id\" as argument.");
			return null;
		}
		return decodedPatterns;
	}

	static class ToString implements Function<Tuple2<Sequence,Integer>, Tuple2<String,Integer>>{
		@Override
		public Tuple2<String, Integer> call(Tuple2<Sequence, Integer> seq) throws Exception {
			return new Tuple2<String, Integer>(Desq.labelToString(hierarchy.getLabelA(Desq.decompressSequence(seq._1))), seq._2);
		}
	}

	static class ToID implements Function<Tuple2<Sequence,Integer>, Tuple2<String,Integer>>{
		@Override
		public Tuple2<String, Integer> call(Tuple2<Sequence, Integer> seq) throws Exception {
			return new Tuple2<String, Integer>(Desq.intsToString(Desq.decompressSequence(seq._1)), seq._2);
		}
	}
	
	//Methods below to be removed, use methods above instead
	
//	public JavaRDD<Tuple2<String,Integer>> decode(){
//		decodedPatterns = patterns.map(new Function<Tuple2<Sequence, Integer>, Tuple2<String,Integer>>(){
//			@Override
//			public Tuple2<String, Integer> call(Tuple2<Sequence, Integer> seq) throws Exception {
//				return new Tuple2<String, Integer>(Desq.labelToString(hierarchy.getLabelA(Desq.decompressSequence(seq._1))), seq._2);
//			}
//		});
//		decoded=true;
//		return decodedPatterns;
//	}
//
//	public JavaRDD<Tuple2<String, Integer>> decodeToIds(){
//		decodedPatterns = patterns.map(new Function<Tuple2<Sequence,Integer>, Tuple2<String,Integer>>(){
//			@Override
//			public Tuple2<String, Integer> call(Tuple2<Sequence, Integer> seq) throws Exception {
//				return new Tuple2<String, Integer>(Desq.intsToString(Desq.decompressSequence(seq._1)), seq._2);
//			}
//		});
//		decoded=true;
//		return decodedPatterns;
//	}
	
	
	/**
	 * Saves the DesqResult and a text file with paths to the content.  
	 * @param sc
	 * @param filePath desired path of new text file
	 * @return an instance loaded from the text file.
	 */
	public DesqResult save(JavaSparkContext sc, String filePath){
		
		String fileSeparator = System.getProperty("file.separator");

		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyMMdd'-'HHmmss");
		String timestamp = format.format(now);
		String dirName = "desq-result"+fileSeparator+timestamp;
		
		File dir = new File(dirName);
		dir.mkdirs();
		
		String hierSavePath = dirName+fileSeparator+"hierarchy";
		String seqSavePath = dirName+fileSeparator+"patterns";
		
		hierarchy.save(sc, hierSavePath, true);
		
		if (decoded){
			JavaRDD<String> decodedString = decodedPatterns.map(new Function<Tuple2<String,Integer>, String>(){
				@Override
				public String call(Tuple2<String,Integer> in){
					return in._1 +"\t"+in._2;
				}
			});
			decodedString.coalesce(1).saveAsTextFile(seqSavePath);
		} else {
			patterns.coalesce(1).saveAsObjectFile(seqSavePath);
		}

		final Charset ENCODING = StandardCharsets.UTF_8;
		
		Path path = Paths.get(filePath);
		
		List<String> lines = new ArrayList<String>();
		lines.add(decoded.toString());
		lines.add(hierSavePath);
		lines.add(seqSavePath);
		
		try {
			Files.write(path, lines, ENCODING);
		} catch (IOException e) {
			e.printStackTrace();
		}
		DesqResult dr = new DesqResult();
		dr.load(sc, filePath);
		return dr;
	}
	
	/**
	 * Loads a DesqResult that was saved with DesqResult.save(). 
	 * @param sc
	 * @param filePath path provided with DesqResult.save()
	 */
	public void load(JavaSparkContext sc, String filePath) {
		
		final Charset ENCODING = StandardCharsets.UTF_8;
		
		Path path = Paths.get(filePath);
		
		List<String> lines = null;
		
		try {
			lines = Files.readAllLines(path, ENCODING);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		//Get contents
		
		String strDecoded = lines.get(0);
		String hierPath = lines.get(1);
		String seqPath = lines.get(2);
		
		//Get status
		decoded = Boolean.valueOf(strDecoded);
		
		//Loading hierarchy with statistics
		try {
			hierarchy = new Hierarchy();
			hierarchy.loadJSON(sc, hierPath, true);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		if(decoded){
			JavaRDD<String> decodedString = sc.textFile(seqPath);
			decodedPatterns = decodedString.map(new Function<String, Tuple2<String,Integer>>(){
				@Override
				public Tuple2<String,Integer> call(String in){
					String[] splits = in.split("\t");
					return new Tuple2<String,Integer>(splits[0], Integer.parseInt(splits[1]));
				}
			});
		}else{
			patterns = sc.objectFile(seqPath);
		}
	}
	
	/**
	 * @return patterns if they are decoded to id's or label's
	 */
	public JavaRDD<Tuple2<String,Integer>> getPatterns(){
		return decodedPatterns;
	}
}
